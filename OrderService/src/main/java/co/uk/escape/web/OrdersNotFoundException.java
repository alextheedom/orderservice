package co.uk.escape.web;

public class OrdersNotFoundException extends Exception {

	private static final long serialVersionUID = 4844671563567748848L;

	@Override
	public String getMessage() {
		return "Order not found";
	}

}
