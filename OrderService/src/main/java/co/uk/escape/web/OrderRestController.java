package co.uk.escape.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.uk.escape.domain.Order;
import co.uk.escape.domain.User;
import co.uk.escape.domain.UserNotFoundException;
import co.uk.escape.service.OrderService;
import co.uk.escape.service.UserService;

@RestController
@RequestMapping("/order")
public class OrderRestController {
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	UserService userService;
	
//	@RequestMapping(method=RequestMethod.GET)
//	public List<Order> getOrders(User user){
//		return orderService.getOrdersForUser(user);
//	}
	
	@RequestMapping(method=RequestMethod.GET, value = "/{username}")
	public List<Order> getOrdersForUser(@PathVariable String username) throws UserNotFoundException, OrdersNotFoundException{		
		User user = userService.getUserByUsername(username);		
		return orderService.getOrdersForUser(user);	
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public void addOrder(@RequestBody Order order){
		orderService.saveOrder(order);	
	}
	
	@ExceptionHandler({UserNotFoundException.class, OrdersNotFoundException.class})
	ResponseEntity<String> handleNotFounds(Exception e){
		return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
	}

}
