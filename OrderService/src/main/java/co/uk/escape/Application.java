package co.uk.escape;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import co.uk.escape.domain.Item;
import co.uk.escape.domain.Order;
import co.uk.escape.domain.OrderRepository;
import co.uk.escape.domain.User;
import co.uk.escape.domain.UserRepository;

@EnableWebMvc
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	OrderRepository orderRepository;
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    @Bean
    public WebMvcConfigurerAdapter mvcViewConfigurer() {
    	return new WebMvcConfigurerAdapter() {
    		@Override
    		public void addViewControllers(ViewControllerRegistry registry){
    			registry.addViewController("/users").setViewName("users");
    		}		
		};
    }
    
    @Bean
    public InitializingBean populateTestData(final UserRepository repository){   	
    	return new InitializingBean(){
			@Override
			public void afterPropertiesSet() throws Exception {
				
				User user = userRepository.findByUsername("atheedom");
				
				List<Item> items = new ArrayList<>();
				items.add(new Item("Cup", "Plain white cup"));
				items.add(new Item("Mug", "Plain white mug"));
								
				Order order = new Order(user, 10, items);
				orderRepository.save(order);
				
		    	System.out.println(orderRepository.findAll());				
					
			}		
    	};
    }

    
    
}
