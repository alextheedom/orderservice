package co.uk.escape.domain;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderRepository extends MongoRepository<Order, Integer>{
	
	Order findOrderById(Integer id);
	
	List<Order> findOrderByUser(User user);
	
}
