package co.uk.escape.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.uk.escape.domain.Order;
import co.uk.escape.domain.OrderRepository;
import co.uk.escape.domain.User;

@Service
public class OrderService {
	
	@Autowired
	OrderRepository orderRepository;
	
	public OrderService(){}
	
	public OrderService(OrderRepository orderRepository){
		this.orderRepository = orderRepository;		
	}
	
	public List<Order> getOrdersForUser(User user){
		return orderRepository.findOrderByUser(user);
	}
	
	public Order getOrderById(Integer id){
		return orderRepository.findOrderById(id);
	}
	
	public Order saveOrder(Order order){
		return orderRepository.save(order);
	}

}
